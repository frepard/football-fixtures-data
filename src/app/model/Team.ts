export class Team {
    id: number;
    name: string;
    activeCompetitions: ActiveCompetition[];
    shortName: string;
    tla: string;
    crestUrl: string;
    address: string;
    phone: string;
    website: string;
    email: string;
    founded: number;
    clubColors: string;
    venue: string;
    squad: Squad[];
    lastUpdated: Date;

    constructor(id?: number, name?: string, activeCompetitions?: ActiveCompetition[], shortName?: string, tla?: string, crestUrl?: string, address?: string, phone?: string,
        website?: string, email?: string, founded?: number, clubColors?: string, venue?: string, squad?: Squad[], lastUpdated?: Date) {
        this.id = id;
        this.name = name;
        this.activeCompetitions = activeCompetitions;
        this.shortName = shortName;
        this.tla = tla;
        this.crestUrl = crestUrl;
        this.address = address;
        this.phone = phone;
        this.website = website;
        this.email = email;
        this.founded = founded;
        this.clubColors = clubColors;
        this.venue = venue;
        this.squad = squad;
        this.lastUpdated = lastUpdated;
    }
}

export interface Area {
    id: number;
    name: string;
}

export interface ActiveCompetition {
    id: number;
    area: Area;
    name: string;
    code: string;
    plan: string;
    lastUpdated: Date;
}

export interface Squad {
    id: number;
    name: string;
    position: string;
    dateOfBirth: Date;
    countryOfBirth: string;
    nationality: string;
    shirtNumber?: any;
    role: string;
}

export interface RootObject {
    id: number;
    area: Area;
    activeCompetitions: ActiveCompetition[];
    name: string;
    shortName: string;
    tla: string;
    crestUrl: string;
    address: string;
    phone: string;
    website: string;
    email: string;
    founded: number;
    clubColors: string;
    venue: string;
    squad: Squad[];
    lastUpdated: Date;
}