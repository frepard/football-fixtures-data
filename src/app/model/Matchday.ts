import { Matches } from './Matches';

export class Matchday{
    currentMatchday: string;
    startDate: string;
    endDate: string;
    matches: Matches[];
    
    constructor(currentMatchday: string, startDate: string, endDate: string){
        this.currentMatchday = currentMatchday;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}