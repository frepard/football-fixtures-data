import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Fixture } from './model/Fixture';
import { Matches } from './model/Matches';
import { Team } from './model/Team';

@Injectable({
  providedIn: 'root'
})
export class FixtureServiceService {

  fixtures: Fixture;
  team: Team;

  constructor(private http: HttpClient) {
    this.team = new Team();
    this.fixtures = new Fixture();
  }

  getAllFixtures(): Observable<Fixture> {
    //8c8735c64c5c418e9928d2bb9d389352 -->Francesco's Key
    //1d9305aa5e1b483ca6f85d97a53fbdf4 -->Simone's Key
    let headers = new HttpHeaders({
      "Content-Type": "application-json",
      "Accept": "application/json",
      "X-Auth-Token": "8c8735c64c5c418e9928d2bb9d389352"
    });
    return this.http.get<Fixture>("http://api.football-data.org/v2/competitions/2019/matches/?season=2019",
      { headers: headers });
  }

  getTeamDetails(id: number): Observable<Team> {
    //8c8735c64c5c418e9928d2bb9d389352 -->Francesco's Key
    //1d9305aa5e1b483ca6f85d97a53fbdf4 -->Simone's Key
    let headers = new HttpHeaders({
      "Content-Type": "application-json",
      "Accept": "application/json",
      "X-Auth-Token": "8c8735c64c5c418e9928d2bb9d389352"
    });
    return this.http.get<Team>("http://api.football-data.org/v2/teams/" + id, { headers: headers });
  }

  getMatchDetails(id: number): Observable<Matches> {
    let headers = new HttpHeaders({
      "Content-Type": "application-json",
      "Accept": "application/json",
      "X-Auth-Token": "8c8735c64c5c418e9928d2bb9d389352"
    });
    return this.http.get<Matches>("http://api.football-data.org/v2/matches/" + id, { headers: headers });
  }

}
