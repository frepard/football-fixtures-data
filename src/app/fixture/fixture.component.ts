import { Component, OnInit, Input } from '@angular/core';
import { Fixture } from '../model/Fixture';
import { FixtureServiceService } from '../fixture-service.service';

@Component({
  selector: 'app-fixture',
  templateUrl: './fixture.component.html',
  styleUrls: ['./fixture.component.css']
})
export class FixtureComponent implements OnInit {

  fixtures: Fixture;

  constructor(private fixtureService: FixtureServiceService) {
    this.fixtures = new Fixture();
  }

  ngOnInit() {
    this.fixtureService.getAllFixtures().subscribe(
      (result: Fixture) => {
        this.fixtures = result;
      },
      (error) => {
        console.error("Failed to load fixture", error);
      }
    );
  }



}
