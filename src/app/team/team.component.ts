import { Component, OnInit, Input } from '@angular/core';
import { FixtureServiceService } from '../fixture-service.service';
import { Team, Squad } from '../model/Team';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})

export class TeamComponent implements OnInit {

  team: Team;

  constructor(
    private fixtureService: FixtureServiceService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.getTeamDetails();
  }

  getTeamDetails(): void {
    const id = + this.route.snapshot.paramMap.get('id');
    this.fixtureService.getTeamDetails(id).subscribe(
      (result: Team) => {
        this.team = result;
      },
      (error) => {
        console.error("Failed to load team details", error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

}
