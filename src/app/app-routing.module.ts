import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { FixtureComponent } from './fixture/fixture.component';
import { TeamComponent } from './team/team.component';
import { MatchDetailsComponent } from './match-details/match-details.component';

const appRoutes: Routes = [
  {
    path: '', redirectTo: 'fixture', pathMatch: 'full'
  },
  {
    path: 'fixture', component: FixtureComponent
  },
  {
    path: 'matches/:id', component: MatchDetailsComponent
  },
  {
    path: 'teams/:id', component: TeamComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
