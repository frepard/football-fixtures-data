import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FixtureComponent } from './fixture/fixture.component';
import { TeamComponent } from './team/team.component';
import { MatchDetailsComponent } from './match-details/match-details.component';
import { MatchdayComponent } from './matchday/matchday.component';
import { FixtureServiceService } from './fixture-service.service';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    FixtureComponent,
    MatchdayComponent,
    MatchDetailsComponent,
    TeamComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [FixtureServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
