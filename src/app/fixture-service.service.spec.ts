import { TestBed } from '@angular/core/testing';

import { FixtureServiceService } from './fixture-service.service';

describe('FixtureServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FixtureServiceService = TestBed.get(FixtureServiceService);
    expect(service).toBeTruthy();
  });
});
